from django.http import HttpResponse
from django.shortcuts import render

def HomePage(request):
    #how to get dictionary
    data={
        'title':'Home new',
        'bdata':'Welcome to the world of Gucci',
        'clist':['Php','Java','python'],
        'number':[],
        'student_details':[
            {'name':'Megha','phone':8839261887},
            {'name':'Nishu','phone':9993116897},
            {'name':'Rishu','phone':9340888358}
        ]
    }
    return render(request,'index.html',data)
    # return render(request,"index.html")
def staticpage(request):
    return render(request,"practise.html")

def staticpage2(request):
    return render(request,"practise2.html")


def AboutUs(request):
    return HttpResponse("Hello ,we provide you worlds best <b>cloths</b>")

def Course(request):
    return HttpResponse("the courses are <b>django</b>") 


def courseDetails(request,courseid):
    return HttpResponse(courseid)       


def userform(request):
    finalans=0
    try:
        # n1=int(request.GET['num1'])
        # n1=int(request.GET['num2'])
        n1=int(request.GET.get('num1'))
        n2=int(request.GET.get('num2'))
        finalans=n1+n2
        # print(n1+n2);
    except:
        pass    
    return render(request,"userform.html",{'output':finalans})           


def userform2(request):
    finalans=0
    data={}
    try:
        if request.method=="POST":
        # n1=int(request.GET['num1'])
        # n1=int(request.GET['num2'])
            n1=int(request.POST.get('num1'))
            n2=int(request.POST.get('num2'))
            finalans=n1+n2
            # data={
            #     'n1'=n1,
            #     'n2'=n2,
            #     'output'=finalans
            # }
        # print(n1+n2);
    except:
        pass    
    return render(request,"userform2.html",{'output':finalans})               